import React from "react";
import { connect } from "react-redux";
import { editMessage } from "../actions/messages";
import "./css/modaledit.css";
import { v4 as uuidv4 } from "uuid";
import { setEditedText } from "../actions/messages";
class ModalEdit extends React.Component {
  editMessage = () => {
    const inputArea = document.querySelector(".edit-message-input");
    const input = inputArea.value;
    const editedMessage = Object.assign(
      { id: this.props.editedMessage, isOwn: true },
      {
        text: input,
        createdAt: new Date(),
      }
    );
    this.props.edit(editedMessage);
  };
  closeModal = () => {
    const id = this.props.editedMessage;
    const message = this.props.messages.find((msg) => msg.id === id);
    this.props.edit(message);
  };
  _handlechange = (e) => {
    this.props.setEditedText(e.target.value);
  };
  render() {
    const data = this.props;
    const classListBlock = data.editing
      ? "edit-message-modal modal-shown"
      : "edit-message-modal";
    return (
      <div className={classListBlock}>
        <label>Enter new text-message:</label>
        <div key={uuidv4()}></div>
        <input
          type="text"
          className="edit-message-input"
          value={this.props.editedMessageText || ""}
          onChange={this._handlechange}
        ></input>
        <button
          className="edit-message-button edit-btn"
          onClick={this.editMessage}
        >
          Change
        </button>
        <button
          className="edit-message-close edit-btn"
          onClick={this.closeModal}
        >
          Close
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    editing: state.chat.editing,
    editedMessage: state.chat.editedMessage,
    messages: state.chat.messages,
    editedMessageText: state.chat.editedMessageText,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    edit: (message) => {
      dispatch(editMessage(message));
    },
    setEditedText: (text) => {
      dispatch(setEditedText(text));
    },
  };
};

const ModalEditContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalEdit);

export default ModalEditContainer;
