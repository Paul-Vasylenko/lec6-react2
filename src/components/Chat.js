import React from "react";
import Preloader from "./Preloader";
import Logo from "./Logo";
import MessageInput from "./MessageInput";
import MessageList from "./MessageList";
import Header from "./Header";
import ModalEdit from "./ModalEdit";
import {
  hidePreloader,
  setMessages,
  startEditingMessage,
} from "../actions/messages";
import { connect } from "react-redux";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    window.addEventListener("keyup", (e) => {
      if (e.key === "ArrowUp") {
        const currentMessages = this.props.messages;
        if (currentMessages) {
          const lastMessage = currentMessages[currentMessages.length - 1];
          if (lastMessage.isOwn) {
            this.props.startEditing(lastMessage.id);
          }
        }
      }
    });
  }
  componentDidMount() {
    fetch(this.props.url)
      .then((res) => res.json())
      .then((json) => {
        json.sort((message1, message2) => {
          if (message1.createdAt < message2.createdAt) return -1;
          if (message1.createdAt === message2.createdAt) return 0;
          if (message1.createdAt > message2.createdAt) return 1;
        });
        this.props.setMessages(json);
        this.props.hidePreloader();
      });
  }
  render = () => {
    const data = this.props;
    const isFetched = data.fetched;
    if (!isFetched) {
      return <Preloader />;
    } else {
      return (
        <div className="chat">
          <Logo />
          <Header />
          <MessageList />
          <MessageInput />
          <ModalEdit />
        </div>
      );
    }
  };
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    fetched: state.chat.fetched,
    editing: state.chat.editing,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setMessages: (messages) => {
      dispatch(setMessages(messages));
    },
    hidePreloader: () => {
      dispatch(hidePreloader());
    },
    startEditing: (messageID) => {
      dispatch(startEditingMessage(messageID));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
