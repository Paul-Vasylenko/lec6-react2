import React from "react";
import "./css/MessageList.css";
import "./css/Message.css";
import "./css/OwnMessage.css";
import moment from "moment";
import { connect } from "react-redux";
import { setMessages, startEditingMessage } from "../actions/messages";

class OwnMessage extends React.Component {
  onDelete = (e) => {
    const messages = this.props.messages.filter((item) => {
      if (item.id !== e.target.parentElement.id) {
        return item;
      }
    });
    this.props.delete(messages);
  };
  editMessage = (e) => {
    const messageID = e.target.parentElement.id;
    this.props.startEditing(messageID);
  };
  render() {
    let message = this.props.message;
    return (
      <div className="message own-message" id={this.props.id}>
        <p className="message-text">{message.text}</p>
        <p className="message-time">{getTimeHHMM(message.createdAt)}</p>
        <i
          className="fas fa-pencil-alt message-edit"
          onClick={this.editMessage}
        ></i>
        <i
          className="fas fa-trash-alt message-delete"
          onClick={this.onDelete}
        ></i>
      </div>
    );
  }
}

export const getTimeHHMM = (data) => {
  const format = "HH:mm";
  var date = new Date(data);
  let dateTime = moment(date).format(format);
  return dateTime;
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    editedMessageText: state.chat.editedMessageText,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    delete: (messages) => {
      dispatch(setMessages(messages));
    },
    startEditing: (messageID) => {
      dispatch(startEditingMessage(messageID));
    },
  };
};

const OwnMessageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(OwnMessage);
export default OwnMessageContainer;
