import React from "react";
import "./css/Header.css";
import moment from "moment";
import { connect } from "react-redux";
class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="header-title">Best chat ever</div>
        <div className="header-users-count">
          {getUsersNum(this.props.messages)}
        </div>
        <div className="header-messages-count">
          {this.props.messages.length}
        </div>
        <div className="header-last-message-date">
          {getFullDate(
            this.props.messages[this.props.messages.length - 1]?.createdAt
          )}
        </div>
      </div>
    );
  }
}

const getFullDate = (data) => {
  const format = "DD.MM.YYYY HH:mm";
  var date = new Date(data);
  let dateTime = moment(date).format(format);
  return dateTime;
};

const getUsersNum = (messages) => {
  const result = [];
  messages.forEach((item) => {
    if (!result.includes(item.user)) result.push(item.user);
  });
  return result.length;
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

const MessageInputContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

export default MessageInputContainer;
