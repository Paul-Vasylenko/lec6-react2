import React from 'react';
import './css/Devider.css';
import moment from 'moment';
class Devider extends React.Component {
    render() {
        return (
            <div className="messages-divider">
                <span>
                    {moment(new Date(this.props.time)).calendar(null, {
                        lastDay: '[Yesterday]',
                        sameDay: '[Today]',
                        sameElse: `dddd, DD MMMM`,
                    })}
                </span>
            </div>
        );
    }
}
export default Devider;
