import React from "react";
import "./css/MessageInput.css";
import { sendMessage } from "../actions/messages";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";

class MessageInput extends React.Component {
  sendMessage = () => {
    const input = document.querySelector(".message-input-text");
    const id = uuidv4();
    const messageText = input.value;
    const TDate = new Date();
    const message = {
      id: id,
      text: messageText,
      createdAt: TDate,
      isOwn: true,
    };

    this.props.send(message);
    input.value = "";
  };
  render() {
    return (
      <div className="message-input">
        <input type="text" className="message-input-text" autoFocus></input>
        <button className="message-input-button" onClick={this.sendMessage}>
          Send
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    editing: state.chat.editing,
    editedMessage: state.chat.editedMessage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    send: (message) => {
      dispatch(sendMessage(message));
    },
  };
};

const MessageInputContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageInput);
export default MessageInputContainer;
